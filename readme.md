# Google Drive Access for Javascript

## Introduction

A helpful library for making requests to the Google Drive API.

## Usage

```
  import { googleDriveAccess } from "https://bitbucket.org/sagal/gdrive_js/raw/master/google_drive_access.js";
```

## Functions

### getGoogleDriveAccessToken(String googleClientId, String googleSecret, String refreshToken) : String

Returns the access token needed for oauth2 authorization, using google credentials as well as a permanent refresh token.

Parameters:

- googleClientId: the client's google ID.
- googleSecret: the client's secret code.
- refreshToken: the permanent refresh token used to grant access tokens.

### getAllFilesFromFolder(String folderName, String accessToken, Object extraParams) : Array<Object>

Returns an array of objects representing the metadata of all files from the first folder with the given name in Google Drive, without the actual files' contents.

Parameters:

- folderName: the name of the folder to be searched.
- accessToken: the access token needed to grant access to the Drive API.
- extraParams: an object with the following options, written as object keys with values:
- - extraParams.queryData: an Array containing all additional queries that should be added to the request for the files.
- - extraParams.fields: any additional fields that should be recieved from the response.
- - extraParams.isPagedRequest: whether the request requires a paginated search (should be used if there are a lot of files to retrieve).

### getSpecificFileFromFolderByName(String fileName, String folderName, String accessToken, Object extraParams) : Object

Returns an object with the contents of the first file found with the given name, within the first folder found with the given folder name.

Parameters:

- fileName: the name of the file to be retrieved.
- folderName: the name of the folder to be searched.
- accessToken: the access token needed to grant access to the Drive API.
- extraParams: an object with the following options, written as object keys with values:
- - extraParams.queryData: an Array containing all additional queries that should be added to the request for the files.
- - extraParams.fields: any additional fields that should be recieved from the response.
- - extraParams.isPagedRequest: whether the request requires a paginated search (should be used if there are a lot of files to retrieve).

### getSpecificFileContentById(String fileId, String accessToken) : Object

Returns an object with the contents of the first file found with the given id.

Parameters:

- fileId: the unique Id of the file to be retrieved.
- accessToken: the access token needed to grant access to the Drive API.
