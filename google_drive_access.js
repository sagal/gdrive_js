export let googleDriveAccess = {
  getGoogleDriveAccessToken: async function (
    googleClientId,
    googleSecret,
    refreshToken
  ) {
    try {
      let urlEncodedData = new URLSearchParams();
      urlEncodedData.append("client_id", googleClientId);
      urlEncodedData.append("client_secret", googleSecret);
      urlEncodedData.append("refresh_token", refreshToken);
      urlEncodedData.append("grant_type", "refresh_token");
      let response = await fetch("https://accounts.google.com/o/oauth2/token", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: urlEncodedData,
      });
      response = await response.json();
      return response.access_token;
    } catch (e) {
      throw new Error(`Authorization failed: ${e.message}`);
    }
  },
  getAllFilesFromFolderById: async function (
    folderId,
    accessToken,
    extraParams
  ) {
    try {
      let fields;
      let response;
      let url = `https://www.googleapis.com/drive/v3/files`;
      let params = `?q='${folderId}' in parents and trashed=false`;
      if (extraParams.queryData) {
        for (let extraQueryInfo of extraParams.queryData) {
          params += ` and ${extraQueryInfo}`;
        }
      }
      if (!extraParams.isPagedRequest) {
        fields = `&fields=files(*)${extraParams.fields ? "," : ""}${
          extraParams.fields ? extraParams.fields : ""
        }`;
        response = await fetch(url + params + fields, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
        response = await response.json();
        return response.files;
      } else {
        let filesToReturn = [];
        let nextPageToken = null;
        let isFirst = true;
        let count = 0;

        while (nextPageToken || isFirst) {
          isFirst = false;
          console.log(
            `Retrieving page ${count} of drive folder ${folderId}...`
          );
          count++;
          fields = `&fields=files(*),nextPageToken${
            extraParams.fields ? "," : ""
          }${extraParams.fields ? extraParams.fields : ""}`;
          let pageToken = nextPageToken ? `&pageToken=${nextPageToken}` : "";
          response = await fetch(url + params + fields + pageToken, {
            method: "GET",
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },
          });
          response = await response.json();
          nextPageToken = response.nextPageToken;
          for (let file of response.files) {
            filesToReturn.push(file);
          }
        }
        return filesToReturn;
      }
    } catch (e) {
      throw new Error(`Failed to retrieve all files from folder: ${e.message}`);
    }
  },
  getAllFilesFromFolder: async function (folderName, accessToken, extraParams) {
    try {
      let url = "https://www.googleapis.com/drive/v3/files";
      let params = `?q=name='${folderName}' and trashed=false`;
      let fields = `&fields=files(*)`;
      let response = await fetch(url + params + fields, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      response = await response.json();
      let folderId = response.files[0].id;
      url = `https://www.googleapis.com/drive/v3/files`;
      params = `?q='${folderId}' in parents and trashed=false`;
      if (extraParams.queryData) {
        for (let extraQueryInfo of extraParams.queryData) {
          params += ` and ${extraQueryInfo}`;
        }
      }
      if (!extraParams.isPagedRequest) {
        fields = `&fields=files(*)${extraParams.fields ? "," : ""}${
          extraParams.fields ? extraParams.fields : ""
        }`;
        response = await fetch(url + params + fields, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
        response = await response.json();
        return response.files;
      } else {
        let filesToReturn = [];
        let nextPageToken = null;
        let isFirst = true;
        let count = 0;

        while (nextPageToken || isFirst) {
          isFirst = false;
          console.log(
            `Retrieving page ${count} of drive folder ${folderName}...`
          );
          count++;
          fields = `&fields=files(*),nextPageToken${
            extraParams.fields ? "," : ""
          }${extraParams.fields ? extraParams.fields : ""}`;
          let pageToken = nextPageToken ? `&pageToken=${nextPageToken}` : "";
          response = await fetch(url + params + fields + pageToken, {
            method: "GET",
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },
          });
          response = await response.json();
          nextPageToken = response.nextPageToken;
          for (let file of response.files) {
            filesToReturn.push(file);
          }
        }
        return filesToReturn;
      }
    } catch (e) {
      throw new Error(`Failed to retrieve all files from folder: ${e.message}`);
    }
  },
  getSpecificFileFromFolderByName: async function (
    fileName,
    folderName,
    accessToken,
    extraParams
  ) {
    try {
      let allFilesInFolder = await this.getAllFilesFromFolder(
        folderName,
        accessToken,
        extraParams
      );

      let fileToRetrieve = allFilesInFolder.find((file) => {
        return file.name == fileName;
      });

      let url = `https://www.googleapis.com/drive/v3/files/${fileToRetrieve.id}`;
      let params = `?alt=media`;
      let response = await fetch(url + params, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      return response;
    } catch (e) {
      throw new Error(`Failed to retrieve file: ${e.message}`);
    }
  },
  getSpecificFileContentById: async function (fileId, accessToken) {
    try {
      let url = `https://www.googleapis.com/drive/v3/files/${fileId}`;
      let params = `?alt=media`;
      let response = await fetch(url + params, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      return response;
    } catch (e) {
      throw new Error(`Failed to retrieve file: ${e.message}`);
    }
  },
};
